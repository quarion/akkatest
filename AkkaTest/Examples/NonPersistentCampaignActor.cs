using System;
using Akka.Actor;
using AkkaTest.Domain.Actors;
using AkkaTest.Domain.Common;
using AkkaTest.Domain.Events;

namespace AkkaTest.Domain.Examples
{
    public class NonPersistentCampaignActor : ReceiveActor, IAggregateRoot
    {
        public string Id { get; }
        public string Name { get; }
        public CampaignStatus Status { get; private set; }

        public NonPersistentCampaignActor(string id, string name)
        {
            Id = id;
            Name = name;
            Receive<Campaign.CreateOfferCommand>(command => CreateOffer(command));
            Receive<Campaign.RejectCampaingCommand>(command => Reject(command));
        }

        private void Reject(Campaign.RejectCampaingCommand command)
        {
            var ev = new CampaignRejectedEvent(Id);

            HandleRejectedEvent(ev);

            Context.System.EventStream.Publish(ev);
            Sender.Tell(Id, Self); // this is probably not needed. Need to figure out how to handle commands in "fire and forget" manner.
        }

        private void CreateOffer(Campaign.CreateOfferCommand createOfferCommand)
        {
            throw new NotImplementedException();
        }

        private void HandleRejectedEvent(CampaignRejectedEvent rejectedEvent)
        {
            this.Status = CampaignStatus.Rejected;
        }

    }
}