namespace AkkaTest.Domain.Common
{
    internal interface IDomainEntity
    {
        string Id { get; }
    }
}