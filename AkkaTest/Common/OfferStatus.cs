namespace AkkaTest.Domain.Common
{
    public enum OfferStatus
    {
        Draft,
        Rejected,
        Active
    }
}