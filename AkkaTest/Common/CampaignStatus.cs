namespace AkkaTest.Domain.Common
{
    public enum CampaignStatus
    {
        Draft,
        Rejected
    }
}