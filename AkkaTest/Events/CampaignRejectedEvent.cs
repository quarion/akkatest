namespace AkkaTest.Domain.Events
{
    public class CampaignRejectedEvent
    {
        public string Id { get; }

        public CampaignRejectedEvent(string id)
        {
            Id = id;
        }
    }
}