﻿using System;
using AkkaTest.Domain.Common;
using AkkaTest.Domain.Framework;

namespace AkkaTest.Domain.Events
{
    // Fully qualified names of event classes are important, because they directly identify them in the event log database. Need to check if there is a way to re-map them after refactoring.

    public class CampaignCreatedEvent : IAggregateIdentifier
    {
        public string Id { get; }

        public string Name { get; }

        public DateTime? CreationDate { get; }

        public CampaignStatus Status { get; }

        public CampaignCreatedEvent(string id, string name, DateTime creationDate, CampaignStatus status)
        {
            Id = id;
            Name = name;
            CreationDate = creationDate;
            Status = status;
        }

        // For extracting aggregate id
        string IAggregateIdentifier.AggregateId => Id;
    }
}