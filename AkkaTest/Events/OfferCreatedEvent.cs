using AkkaTest.Domain.Framework;

namespace AkkaTest.Domain.Events
{
    public class OfferCreatedEvent : IAggregateIdentifier
    {
        public string OfferId { get; private set; }
        public string CampaignId { get; private set; }

        public OfferCreatedEvent(string offerId, string campaignId)
        {
            OfferId = offerId;
            CampaignId = campaignId;
        }

        string IAggregateIdentifier.AggregateId => OfferId;
    }
}