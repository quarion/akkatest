using AkkaTest.Domain.Common;

namespace AkkaTest.Domain.Events
{
    public class OfferDefinitionUpdatedEvent
    {
        public string OfferId { get; set; }
        public OfferStatus Status { get; set; }
        public string TriggerItemId { get; set; }
        public int AmountOfPointsToGrant { get; set; }
    }
}