namespace AkkaTest.Domain.Events
{
    public class OfferRejectedEvent
    {
        public string Id { get; }

        public OfferRejectedEvent(string id)
        {
            Id = id;
        }
    }
}