using System;
using System.Linq;
using Akka.Actor;
using Akka.Persistence;
using AkkaTest.Domain.Common;
using AkkaTest.Domain.Events;
using AkkaTest.Domain.Framework;

namespace AkkaTest.Domain.Actors
{
    public class Campaign : ReceivePersistentActor
    {
        public override string PersistenceId { get; } //possibly this Id should be different from aggregate id. Persistence ID should be globally uniqe, while aggregate id only needs to be unique for this aggregate type

        private string Name { get; set; }
        private CampaignStatus Status { get; set; }

        private int _offersCount;

        public Campaign()
        {
            PersistenceId = Self.Path.Name;

            Command<CampaignCreatedEvent>(ev => Initialize(ev));
            Recover<CampaignCreatedEvent>(ev => Handle(ev));

            Command<RejectCampaingCommand>(command => Reject(command));
            Recover<CampaignRejectedEvent>(ev => Handle(ev));

            Command<CreateOfferCommand>(command => CreateOffer(command));
            Recover<OfferCreatedEvent>(ev => Handle(ev));
        }

        private void Initialize(CampaignCreatedEvent @event)
        {
            Persist(@event, ev =>
            {
                Handle(ev);
                Context.System.EventStream.Publish(ev);
            });
        }

        private void Reject(RejectCampaingCommand command)
        {
            // There seems to be a good patter for commands:
            // Check pre-conditions
            // Prepare events
            // Persist events
            // On succes, apply events to state of this aggregate
            // Send replay if needed

            if (Status == CampaignStatus.Rejected)
            {
                return;
            }

            var @event = new CampaignRejectedEvent(PersistenceId);

            Persist(@event, ev =>
            {
                Handle(ev);
                Context.System.EventStream.Publish(ev);

                // Tell all offers in this campaign to also reject. This seems to be better way to do it, when using event sourcing, than to use broadcasting. Here we could guarantee delivery with some extra coding
                // Need to consider if this covers all cases
                //Context.ActorSelection("./*").Tell(ev); //Tell all children
            });
        }

        private void CreateOffer(CreateOfferCommand createOfferCommand)
        {
            if(Status == CampaignStatus.Rejected)
                throw new InvalidOperationException("Campaign is rejected and new offers cannot be created"); // Need to find a correct way to do error handling

            
            //var offerId = PersistenceId + ":offer-" + _offersCount;
            var offerId = Guid.NewGuid().ToString(); //PersistenceId + ":offer-" + _offersCount;
            var @event = new OfferCreatedEvent(offerId: offerId, campaignId: PersistenceId);

            var offer = Offices.Get<Offer>();
            offer.Tell(@event);


            Sender.Tell(@event.OfferId, Self);
        }

        private void Handle(CampaignCreatedEvent @event)
        {
            Name = @event.Name;
            Status = @event.Status;
        }

        private void Handle(CampaignRejectedEvent rejectedEvent)
        {
            this.Status = CampaignStatus.Rejected;
        }

        private void Handle(OfferCreatedEvent @event)
        {
            /*var campaignActor = Context.ActorOf(Props.Create(() => new Offer(@event.OfferId, PersistenceId)),
                @event.OfferId);*/

            _offersCount += 1;
        }

        public class RejectCampaingCommand
        {
        }

        public class CreateOfferCommand/* : IAggregateIdentifier*/
        {
            /*public string CampaignId { get; }

            public CreateOfferCommand(string campaignId)
            {
                CampaignId = campaignId;
            }

            string IAggregateIdentifier.AggregateId => CampaignId;*/
        }
    }
}