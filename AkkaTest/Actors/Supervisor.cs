using System;
using System.Linq;
using Akka.Actor;
using Akka.Event;
using Akka.Persistence;
using AkkaTest.Domain.Common;
using AkkaTest.Domain.Events;
using AkkaTest.Domain.Framework;

namespace AkkaTest.Domain.Actors
{
    // In current state, this is only a bottleneck for creating campaigns, and it do not protect any invariants
    // Need to work on the Id generation strategy to not have this kind of bottleneck, while having nice human readable IDs

    public class Supervisor : ReceivePersistentActor
    {
        public override string PersistenceId => "supervisor-id";

        private readonly ILoggingAdapter _log = Context.GetLogger();
        //private int _campaignsCount = 0;

        public Supervisor()
        {
            Command<CreateCampaignCommand>(command => CreateCampaign(command));
        }

        private void CreateCampaign(CreateCampaignCommand command)
        {
            //var id = "camp-" + _campaignsCount;
            // To be more user friendly, we could extend this actor or create new"id provider" actor.
            // To do that we need to implement a query to restore the sequence number
            // Better idea - use guid as technical id, but create "user id" that will be populated by read side and used ony for displaying
            var id = command.Id ?? "camp-" + Guid.NewGuid().ToString().Split('-').First();


            _log.Info($"Creating campaign \"{command.Name}\" with id {id}");


            var campaignCreatedEvent = new CampaignCreatedEvent(id, command.Name, DateTime.UtcNow, CampaignStatus.Draft); 

            var campaignOffice = Offices.Get<Campaign>();
            campaignOffice.Tell(campaignCreatedEvent);

            Sender.Tell(id, Self); //With this flow we do not wait for the command to be executed (Campaign to be persisted). This may cause overloadign - we would queue too many messages and some of them will fail with timeout
        }

        public class CreateCampaignCommand
        {
            public string Name { get; }

            public string Id { get; }

            public CreateCampaignCommand(string name, string id = null)
            {
                Name = name;
                Id = id;
            }
        }
    }
}