using System;
using Akka.Persistence;
using AkkaTest.Domain.Common;
using AkkaTest.Domain.Events;

namespace AkkaTest.Domain.Actors
{
    /// <summary>
    /// Simple offer that will grant some loyalty points when buying specific item
    /// </summary>
    public class Offer : ReceivePersistentActor
    {
        public override string PersistenceId { get; }

        private string ParentCampaignId { get; set; }
        private OfferStatus Status { get; set; }
        private string TriggerItemId { get; set; }
        private int AmountOfPointsToGrant {get; set;}

        public Offer()
        {
            PersistenceId = Self.Path.Name;

            Command<OfferCreatedEvent>(ev => Initialize(ev));
            Recover<OfferCreatedEvent>(ev => Handle(ev));

            Command<CampaignRejectedEvent>(ev => Reject(ev)); // this command is triggered by event. Need to consider if this is best structure. In classical approach this is done by "event processor"
            Recover<OfferRejectedEvent>(ev => Handle(ev));
            Context.System.EventStream.Subscribe(Self, typeof(CampaignRejectedEvent));

            Command<UpdateOfferDefinitionCommand>(c => UpdateDefinition(c));
            Recover<OfferDefinitionUpdatedEvent>(ev => Handle(ev));
        }

        private void Initialize(OfferCreatedEvent @event)
        {
            Persist(@event, ev =>
            {
                Handle(ev);
                Context.System.EventStream.Publish(ev);
            });
        }

        private void UpdateDefinition(UpdateOfferDefinitionCommand command)
        {
            if(Status != OfferStatus.Draft)
                throw new InvalidOperationException("Only draft offers can be changed");

            var @event = new OfferDefinitionUpdatedEvent()
            {
                OfferId = PersistenceId,
                Status = command.Status,
                TriggerItemId = command.TriggerItemId,
                AmountOfPointsToGrant = command.AmountOfPointsToGrant
            };

            Persist(@event, ev =>
            {
                Handle(ev);
                Context.System.EventStream.Publish(ev);
            });
        }

        private void Reject(CampaignRejectedEvent command) //maybe this parameter is not needed. Or we should validate that we do not get events for some other campaign, that is not a parent?
        {
            // When parent campaign is rejected, also reject the offer

            if (command.Id != ParentCampaignId || Status == OfferStatus.Rejected)
                return;

            var @event = new OfferRejectedEvent(PersistenceId);

            Persist(@event, ev =>
            {
                Handle(ev);
                Context.System.EventStream.Publish(ev);
            });
        }

        private void Handle(OfferCreatedEvent @event)
        {
            ParentCampaignId = @event.CampaignId;
            Status = OfferStatus.Draft;
        }

        private void Handle(OfferDefinitionUpdatedEvent @event)
        {
            Status = @event.Status;
            AmountOfPointsToGrant = @event.AmountOfPointsToGrant;
            TriggerItemId = @event.TriggerItemId;
        }

        private void Handle(OfferRejectedEvent @event)
        {
            Status = OfferStatus.Rejected;
        }

        public class UpdateOfferDefinitionCommand
        {
            public OfferStatus Status { get; set; }
            public string TriggerItemId { get; set; }
            public int AmountOfPointsToGrant { get; set; }
        }
    }
}