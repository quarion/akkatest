﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Akka.Actor;
using Akka.Event;

namespace AkkaTest.Domain.Framework
{
    // I think I do not like term "office" here. "Supervisor" would be better, but it is already taken. Need to think about this

    //For now, assuming single execution node. For multiple need to try to re-introduce sharding or cluster singletones
    public static class Offices
    {
        /*private static readonly Dictionary<Type, object> OfficesCollection = new Dictionary<Type, object>();

        public static void RegisterOffice<TAggregate>(AggregateOffice<TAggregate> office) where TAggregate : ActorBase, new()
        {
            OfficesCollection.Add(typeof(TAggregate), office);
        }

        public static AggregateOffice<TAggregate> Get<TAggregate>() where TAggregate : ActorBase, new()
        {
            return (AggregateOffice<TAggregate>) OfficesCollection[typeof(TAggregate)];
        }*/

        private static readonly Dictionary<Type, IActorRef> OfficesCollection = new Dictionary<Type, IActorRef>();

        public static void RegisterOffice<TAggregate>(IActorRef office) where TAggregate : ActorBase, new()
        {
            OfficesCollection.Add(typeof(TAggregate), office);
        }

        public static IActorRef Get<TAggregate>() where TAggregate : ActorBase, new()
        {
            return OfficesCollection[typeof(TAggregate)];
        }
    }


    // As this holds aggregates as children, it should be a singleton.
    public class AggregateOffice<TAggregate> : ReceiveActor where TAggregate : ActorBase, new()
    {
        // This class in the future should support passivation of actors that are not recieving messages to save memory.
        // But maybe we will get this from integration with Cluster.Sharding

        private readonly ILoggingAdapter _log = Context.GetLogger();

        public AggregateOffice()
        {
            ReceiveAny(Handle);
        }

        private void Handle(object message)
        {
            var command = message as IAggregateIdentifier;
            if (command == null)
            {
                _log.Warning($"Office {Self.Path} recieved unrecognized message");
                return;
            }

            var aggregate = GetAggregate(command.AggregateId);



            if(message is CommandEnvelope envelope) // C# 7 Pattern matching!!!!
                aggregate.Forward(envelope.Message);
            else
                aggregate.Forward(message);
        }

        private IActorRef GetAggregate(string aggregateId)
        {
            var child = Context.Child(aggregateId);

            if(child.Equals(ActorRefs.Nobody))
                child = Context.ActorOf<TAggregate>(aggregateId);

            return child;
        }
    }
}