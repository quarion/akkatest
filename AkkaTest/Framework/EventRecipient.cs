﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akka.Persistence;

namespace AkkaTest.Domain.Framework
{
    //Recive events from pub-sub, de-duplicate them, persists them, and broadcast to local bus
    class EventRecipient : ReceivePersistentActor
    {
        public override string PersistenceId => "event-recipient";


    }
}
