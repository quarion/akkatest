﻿namespace AkkaTest.Domain.Framework
{
    public class CommandEnvelope<TMessage> : IAggregateIdentifier
    {
        public string AggregateId { get; }

        public TMessage Message { get; }

        public CommandEnvelope(string aggregateId, TMessage message)
        {
            AggregateId = aggregateId;
            Message = message;
        }
    }

    public class CommandEnvelope: IAggregateIdentifier
    {
        public string AggregateId { get; }

        public object Message { get; }

        public CommandEnvelope(string aggregateId, object message)
        {
            AggregateId = aggregateId;
            Message = message;
        }
    }
}