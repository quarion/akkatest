﻿namespace AkkaTest.Domain.Framework
{
    interface IAggregateIdentifier
    {
        string AggregateId { get; }
    }
}