using System;

namespace AkkaTest.Domain.Views
{
    public class CampaignDashboardEntry
    {
        public string Id { get; }
        public string Name { get; }
        public DateTime? CreationDate { get; }
        public DateTime? LastUpdateDate { get; }
        public string CampaignStatus { get; }

        public CampaignDashboardEntry(string id, string name, DateTime? creationDate, string campaignStatus, DateTime? lastUpdateDate)
        {
            Id = id;
            Name = name;
            CreationDate = creationDate;
            CampaignStatus = campaignStatus;
            LastUpdateDate = lastUpdateDate;
        }
    }
}