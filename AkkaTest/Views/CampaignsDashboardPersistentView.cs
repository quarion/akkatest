using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using Akka.Actor;
using Akka.Persistence;
using Akka.Persistence.Query;
using Akka.Persistence.Query.Sql;
using Akka.Streams;
using AkkaTest.Domain.Common;
using AkkaTest.Domain.Events;

namespace AkkaTest.Domain.Views
{
    // Creating views this way is problematic, because it wont be re-hydrated with events already persisted in the data store
    // Also, with this implementation all data will be loaded to memory, and this is not efficent. WE should be able to support "paging"
    // To do this, probably this actor could just use normal DB connection, and not store the state inside

    public class CampaignsDashboardPersistentView : ReceivePersistentActor
    {
        public override string PersistenceId => "dashboard-id";

        private readonly Dictionary<string, CampaignDashboardEntry> _dashboardEntries = new Dictionary<string, CampaignDashboardEntry>();

        public CampaignsDashboardPersistentView()
        {
            Command<CampaignCreatedEvent>(ev =>
            {
                Persist(ev, Handle);
            });
            Recover<CampaignCreatedEvent>(ev => Handle(ev));
            //Context.System.EventStream.Subscribe(Self, typeof(CampaignCreatedEvent)); //disabled for now, as it is not optimized


            Command<CampaignRejectedEvent>(ev =>
            {
                Persist(ev, Handle);
            });
            Recover<CampaignRejectedEvent>(ev => Handle(ev));
            //Context.System.EventStream.Subscribe(Self, typeof(CampaignRejectedEvent));        
            

            Command<GetDashboardQuery>(query => ExecuteQuery());
        }

        private void ExecuteQuery()
        {
            var response = _dashboardEntries.Values.ToImmutableList();
            Sender.Tell(response, Self);
        }

        private void Handle(CampaignRejectedEvent ev)
        {
            var dashboardEntry = _dashboardEntries[ev.Id];

            _dashboardEntries[ev.Id] = new CampaignDashboardEntry(
                id: dashboardEntry.Id,
                name: dashboardEntry.Name,
                creationDate: dashboardEntry.CreationDate,
                lastUpdateDate: DateTime.UtcNow, 
                campaignStatus: CampaignStatus.Rejected.ToString());
        }

        private void Handle(CampaignCreatedEvent ev)
        {
            _dashboardEntries.Add(ev.Id, new CampaignDashboardEntry(
                id: ev.Id,
                name: ev.Name,
                creationDate: ev.CreationDate,
                lastUpdateDate: ev.CreationDate,
                campaignStatus: ev.Status.ToString()));
        }

        public class GetDashboardQuery
        {
        }
    }

    /*public class CampaignsDashboardQuery : ReceiveActor
    {
        private readonly Dictionary<string, CampaignDashboardEntry> _dashboardEntries = new Dictionary<string, CampaignDashboardEntry>();

        public CampaignsDashboardQuery()
        {
            Receive<CampaignCreatedEvent>(ev => Handle(ev));

            Receive<CampaignRejectedEvent>(ev => Handle(ev));

            Receive<GetDashboardQuery>(query => ExecuteQuery());
        }

        private void SubscribeToEventStore()
        {
            // obtain read journal by plugin id
            var readJournalPluginId = SqlReadJournal.Identifier;

            var readJournal = PersistenceQuery.Get(Context.System)
                .ReadJournalFor<SqlReadJournal>(readJournalPluginId);

            //var persistenceIds = readJournal.AllPersistenceIds().

            // issue query to journal
            var source = readJournal //to implement this, we probably need tagging of events by aggregate type
                //.EventsByPersistenceId(MarketingManager.GlobalPersistenceId, 0, long.MaxValue);

  

            // materialize stream, consuming events
            var mat = ActorMaterializer.Create(system);
            source.RunForeach(envelope =>
            {
                Console.WriteLine($"event {envelope}");
                //result.Add(envelope.Event as DiscountCreatedEvent);
            }, mat);
        }

        private void ExecuteQuery()
        {
            var response = _dashboardEntries.Values.ToImmutableList();
            Sender.Tell(response, Self);
        }

        private void Handle(CampaignRejectedEvent ev)
        {
            var dashboardEntry = _dashboardEntries[ev.Id];

            _dashboardEntries[ev.Id] = new CampaignDashboardEntry(
                id: dashboardEntry.Id,
                name: dashboardEntry.Name,
                creationDate: dashboardEntry.CreationDate,
                lastUpdateDate: DateTime.UtcNow,
                campaignStatus: CampaignStatus.Rejected.ToString());
        }

        private void Handle(CampaignCreatedEvent ev)
        {
            _dashboardEntries.Add(ev.Id, new CampaignDashboardEntry(
                id: ev.Id,
                name: ev.Name,
                creationDate: ev.CreationDate,
                lastUpdateDate: ev.CreationDate,
                campaignStatus: ev.Status.ToString()));
        }

        public class GetDashboardQuery
        {
        }
    }*/
}