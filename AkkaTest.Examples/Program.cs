﻿using System;
using Akka.Actor;
using AkkaTest.Domain.Actors;
//using Akka.Persistence.Sqlite;

namespace AkkaTest.Examples
{
    class Program
    {
        static void Main(string[] args)
        {
            Tracer.Trace("Starting application...");

            var system = ActorSystem.Create("ActorSystem");
            //SqlitePersistence.Get(system);

            var supervisorActor = system.ActorOf<Supervisor>("supervisor");


            CreateCampaignSync(supervisorActor);



            Console.ReadLine();
        }

        private static void CreateCampaignAsyncWithInbox(IInboxable inbox, IActorRef supervisorActor)
        {
            inbox.Send(supervisorActor, new Supervisor.CreateCampaignCommand("new campaign"));

            try
            {
                var result = inbox.Receive(TimeSpan.FromSeconds(1)); //
                Tracer.Trace($"Got response: campaign was created with id {result}");
            }
            catch (TimeoutException)
            {
                Tracer.Trace($"Timeout - no response");
            }
        }

        private static void CreateCampaignSync(ICanTell supervisorActor)
        {
            var future = supervisorActor.Ask(new Supervisor.CreateCampaignCommand("new campaign"), timeout: TimeSpan.FromSeconds(1)); //TODO: how to handle timeout?
            future.Wait(); //to make it non blocking, this could be awaited in background
            var result = future.Result;

            Tracer.Trace($"Got response: campaign was created with id {result}");
        }
    }
}
