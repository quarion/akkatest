﻿using System;
using System.Diagnostics;

namespace AkkaTest.Examples
{
    static class Tracer
    {
        public static void Trace(string message)
        {
            Debug.WriteLine(message);
            Console.WriteLine(message);
        }
    }
}