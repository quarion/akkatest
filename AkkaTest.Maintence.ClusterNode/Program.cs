﻿using System;
using Topshelf;

namespace AkkaTest.Maintence.ClusterNode
{
    class Program
    {
        static void Main(string[] args)
        {
            /*var actorSystem = ActorSystem.Create("MaintenenceSystem");
          
            var supervisorActor = actorSystem.ActorOf<Supervisor>("supervisor");

            Console.WriteLine("Node is running...");
            Console.ReadLine();*/

            HostFactory.Run(x =>                                 
            {
                x.Service<MaintenenceNodeService>();

                x.SetServiceName("Maintenence node");
                //x.SetDisplayName("Akka.NET Crawl Tracker");
                //x.SetDescription("Akka.NET Cluster Demo - Web Crawler.");

                x.UseAssemblyInfoForServiceInfo();
                x.RunAsLocalSystem();
                x.StartAutomatically();
                x.UseNLog();
                x.EnableServiceRecovery(r => r.RestartService(1));
            });
        }
    }
}