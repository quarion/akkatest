﻿using Akka.Actor;
using Akka.Persistence.Sqlite;
using Akka.Persistence.SqlServer;
using AkkaTest.Domain.Actors;
using AkkaTest.Domain.Framework;
using AkkaTest.Domain.Views;
using Topshelf;

namespace AkkaTest.Maintence.ClusterNode
{
    internal class MaintenenceNodeService : ServiceControl
    {
        private ActorSystem _actorSystem;

        public bool Start(HostControl hostControl)
        {
            _actorSystem = ActorSystem.Create("MaintenenceSystem");

            _actorSystem.ActorOf<Supervisor>("supervisor");
            _actorSystem.ActorOf<CampaignsDashboardPersistentView>("campaignDashboard");

            RegisterOffice<Campaign>("campaignOffice");
            RegisterOffice<Offer>("offerOffice");

            //SqlitePersistence.Get(_actorSystem);
            SqlServerPersistence.Get(_actorSystem);

            _actorSystem.Log.Debug("Node is running...");
            return true;
        }

        private void RegisterOffice<TAggregate>(string name) where TAggregate : ActorBase, new()
        {
            Offices.RegisterOffice<TAggregate>(_actorSystem.ActorOf<AggregateOffice<TAggregate>>(name));
        }

        public bool Stop(HostControl hostControl)
        {
            //shutdown cluster ?
            return true;
        }
    }
}