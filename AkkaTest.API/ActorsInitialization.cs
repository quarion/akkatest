using Akka.Actor;
using Akka.Persistence.Sqlite;
using Akka.Routing;
using AkkaTest.Domain.Actors;
using AkkaTest.Domain.Common;
using AkkaTest.Domain.Events;
using AkkaTest.Domain.Framework;
using AkkaTest.Domain.Views;
using Microsoft.Extensions.DependencyInjection;

namespace AkkaTest.API
{
    public class ActorsInitialization
    {
        public static void InitializeActors(IServiceCollection services)
        {
            var actorSystem = ActorSystem.Create("MaintenenceSystem");

           // var commandRouter = actorSystem.ActorOf(Props.Empty.WithRouter(FromConfig.Instance), "commandRouter");

            // create actor, so it can be later referenced
            // This will also trigger asynchronus restore of the actors when using Akka.persistence
            var supervisorProps = Props.Create<Supervisor>().WithRouter(FromConfig.Instance);
            var supervisorActor = actorSystem.ActorOf(supervisorProps, "supervisor");
            
            //var dashboardActor = actorSystem.ActorOf<CampaignsDashboardView>("campaignDashboard");
            var dashboardActor = actorSystem.ActorOf(
                Props.Create<CampaignsDashboardPersistentView>().WithRouter(FromConfig.Instance),
                "campaignDashboard");

            actorSystem.ActorOf(
                Props.Create<AggregateOffice<Campaign>>().WithRouter(FromConfig.Instance),
                "campaignOffice");


            services.AddSingleton<ActorSystem>(provider => actorSystem);


            SqlitePersistence.Get(actorSystem);
        }
    }
}