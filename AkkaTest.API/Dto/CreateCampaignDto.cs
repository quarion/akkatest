﻿namespace AkkaTest.API.Dto
{
    public class CreateCampaignDto
    {
        public string CampaignId { get; }

        public string CampaignName { get; }


        public CreateCampaignDto(string campaignName, string campaignId = null)
        {
            CampaignName = campaignName;
            CampaignId = campaignId;
        }
    }
}