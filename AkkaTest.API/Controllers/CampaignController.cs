﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Akka.Actor;
using AkkaTest.API.Dto;
using AkkaTest.Domain.Actors;
using AkkaTest.Domain.Examples;
using AkkaTest.Domain.Framework;
using AkkaTest.Domain.Views;
using Microsoft.AspNetCore.Mvc;

namespace AkkaTest.API.Controllers
{
    // The code inside controllers could be handled by application layer. 

    [Route("api/campaign")]
    public class CampaignController : Controller
    {
        private readonly ActorSystem _actorSystem;

        public CampaignController(ActorSystem actorSystem)
        {
            _actorSystem = actorSystem;
        }

        [HttpPost]
        public async Task<string> Post([FromBody]CreateCampaignDto dto)
        {     
            var supervisorActor = _actorSystem.ActorSelection("user/supervisor");

            try
            {
                var future = supervisorActor.Ask(new Supervisor.CreateCampaignCommand(dto.CampaignName, dto.CampaignId), timeout: TimeSpan.FromSeconds(20)); 
                await future;
                return $"Created campaign with id {future.Result}";

            }
            catch (TimeoutException) //this is not working
            {
                return "Timeout";
            }
        }

        [HttpPost("{id}/createOffer")]
        public async Task<string> Post(string id, [FromBody]CreateOfferDto dto)
        {
            var campaignActor = _actorSystem.ActorSelection("user/campaignOffice");

            //var future = campaignActor.Ask(new Campaign.CreateOfferCommand(), timeout: TimeSpan.FromSeconds(5));
            var command = new CommandEnvelope(id, new Campaign.CreateOfferCommand());
            var future = campaignActor.Ask(command, timeout: TimeSpan.FromSeconds(20));
            await future;
            return $"Created offer with id {future.Result}";
        }

        [HttpPut("{id}/reject")]
        public void Put(string id)
        { 
            var office = _actorSystem.ActorSelection($"user/campaignOffice");

            var command = new CommandEnvelope(id, new Campaign.RejectCampaingCommand());
            office.Tell(command); // in this case we do not have any sync acknowledgement from the system
        }

        [HttpGet]
        public async Task<IEnumerable<CampaignDashboardEntry>> Get()
        {
            var dashboard = _actorSystem.ActorSelection("user/campaignDashboard");

            var future = dashboard.Ask(new CampaignsDashboardPersistentView.GetDashboardQuery(), timeout: TimeSpan.FromSeconds(10));
            await future;

            return future.Result as IEnumerable<CampaignDashboardEntry>;
        }
    }
}