﻿using System;
using Akka.Actor;
using Akka.TestKit;
using Akka.TestKit.NUnit;
using AkkaTest.Domain.Actors;
using AkkaTest.Domain.Common;
using AkkaTest.Domain.Events;
using NUnit.Framework;

namespace Domain.Tests
{
    [TestFixture]
    public class CampaignTests : TestKit
    {
        private TestActorRef<Campaign> CreateCampaignActor()
        {
            var campaignProps = Props.Create(() => new Campaign());
            var campaignActor = ActorOfAsTestActorRef<Campaign>(campaignProps, "camp-id"); //this should be avoided. Need to find different way to test
            return campaignActor;
        }

        [Test]
        public void Campaign_GivenItIsADraft_CanCreateNewOffers()
        {
            var campaignActor = CreateCampaignActor();


            campaignActor.Tell(new Campaign.CreateOfferCommand());


            var result = ExpectMsg<string>();
            Assert.That(result, Is.Not.Empty);
        }

        [Test]
        public void Campaign_WhenItIsRejected_WillRejectAllChildOffers()
        {
            var probe = CreateTestProbe();
            Sys.EventStream.Subscribe(probe, typeof(OfferRejectedEvent));

            var campaignActor = CreateCampaignActor();

            campaignActor.Tell(new Campaign.CreateOfferCommand());
            var offerId = ExpectMsg<string>();


            campaignActor.Tell(new Campaign.RejectCampaingCommand());


            probe.ExpectMsg<OfferRejectedEvent>();

            // What we need here, is to assert that "Offer rejected event" was emitted?
            // Or just that "Campaign rejected event" was emitted?

            /*var offer = Sys.ActorSelection($"camp-id/{offerId}");

            Assert.That();*/
        }
    }
}
