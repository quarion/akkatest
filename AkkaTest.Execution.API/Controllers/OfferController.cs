using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Akka.Actor;
using AkkaTest.Domain.Views;
using Microsoft.AspNetCore.Mvc;

namespace AkkaTest.API.Controllers
{
    [Route("api/[controller]")]
    public class OfferController : Controller
    {
        private readonly ActorSystem _actorSystem;

        public OfferController(ActorSystem actorSystem)
        {
            _actorSystem = actorSystem;
        }

       /* [HttpGet]
        public async Task<IEnumerable<CampaignDashboardEntry>> Get()
        {
            var dashboard = _actorSystem.ActorSelection("user/campaignDashboard");

            var future = dashboard.Ask(new CampaignsDashboardView.GetDashboardQuery(), timeout: TimeSpan.FromSeconds(10));
            await future;

            return future.Result as IEnumerable<CampaignDashboardEntry>;
        }*/
    }
}