using Akka.Actor;
using Microsoft.Extensions.DependencyInjection;

namespace AkkaTest.Execution.API
{
    public class ActorsInitialization
    {
        public static void InitializeActors(IServiceCollection services)
        {
            var actorSystem = ActorSystem.Create("executionSystem");

           /* var supervisorActor = actorSystem.ActorOf<Supervisor>("supervisor"); //create actor, so it can be later referenced
            var dashboardActor = actorSystem.ActorOf<CampaignsDashboardView>("campaignDashboard");*/


            services.AddSingleton<ActorSystem>(provider => actorSystem);


           // SqlitePersistence.Get(actorSystem);
        }
    }
}