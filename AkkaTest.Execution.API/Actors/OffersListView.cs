﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using Akka.Persistence;
using AkkaTest.Domain.Common;
using AkkaTest.Domain.Events;
using AkkaTest.Domain.Views;

namespace AkkaTest.Execution.API.Actors
{
    public class OffersListView : ReceivePersistentActor
    {
        public override string PersistenceId => "list-view-id";

        private readonly Dictionary<string, CampaignDashboardEntry> _dashboardEntries = new Dictionary<string, CampaignDashboardEntry>();

        public OffersListView()
        {
            Command<CampaignCreatedEvent>(ev =>
            {
                Persist(ev, Handle);
            });
            Recover<CampaignCreatedEvent>(ev => Handle(ev));
            Context.System.EventStream.Subscribe(Self, typeof(CampaignCreatedEvent));


            Command<CampaignRejectedEvent>(ev =>
            {
                Persist(ev, Handle);
            });
            Recover<CampaignRejectedEvent>(ev => Handle(ev));
            Context.System.EventStream.Subscribe(Self, typeof(CampaignRejectedEvent));


            Command<GetDashboardQuery>(query => ExecuteQuery());
        }

        private void ExecuteQuery()
        {
            var response = _dashboardEntries.Values.ToImmutableList();
            Sender.Tell(response, Self);
        }

        private void Handle(CampaignRejectedEvent ev)
        {
            var dashboardEntry = _dashboardEntries[ev.Id];

            _dashboardEntries[ev.Id] = new CampaignDashboardEntry(
                id: dashboardEntry.Id,
                name: dashboardEntry.Name,
                creationDate: dashboardEntry.CreationDate,
                lastUpdateDate: DateTime.UtcNow,
                campaignStatus: CampaignStatus.Rejected.ToString());
        }

        private void Handle(CampaignCreatedEvent ev)
        {
            _dashboardEntries.Add(ev.Id, new CampaignDashboardEntry(
                id: ev.Id,
                name: ev.Name,
                creationDate: ev.CreationDate,
                lastUpdateDate: ev.CreationDate,
                campaignStatus: ev.Status.ToString()));
        }

        public class GetDashboardQuery
        {
        }
    }
}
