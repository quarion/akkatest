using AkkaTest.Domain.Common;

namespace AkkaTest.Execution.API.Actors
{
    public class OfferListEntry
    {
        public string OfferId { get; set; }
        public OfferStatus Status { get; set; }
        public string TriggerItemId { get; set; }
        public int AmountOfPointsToGrant { get; set; }

        public OfferListEntry(string offerId, OfferStatus status, string triggerItemId, int amountOfPointsToGrant)
        {
            OfferId = offerId;
            Status = status;
            TriggerItemId = triggerItemId;
            AmountOfPointsToGrant = amountOfPointsToGrant;
        }
    }
}